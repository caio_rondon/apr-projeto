/*!
 * Validator v0.11.9 for Bootstrap 3, by @1000hz
 * Copyright 2017 Cina Saffary
 * Licensed under http://opensource.org/licenses/MIT
 *
 * https://github.com/1000hz/bootstrap-validator
 */

+function ($) {
  'use strict';

  $('#formConsulta').on('submit', function(e){
    e.preventDefault();

    var url = "/consulta";


    var posting = $.post( url, {login:$('#loginConsulta').val(), senha:$('#senhaConsulta').val(), cpf_cnpj:$('#cpf_cnpjConsulta').val(), renavam:$('#renavamConsulta').val()} );

    $('#modalCarregando').modal('show');

    posting.done(function( data ) {
      console.log(data);
      $('#span_placa_Consulta').text(data.placa);
      $('#span_renavam_Consulta').text(data.renavam);
      $('#span_cpf_cnpj_Consulta').text(data.cpf_cnpj);
      $('#nome_proprietarioConsulta').text(data.nome_proprietario);
      $('#tipoConsulta').text(data.tipo);
      $('#especieConsulta').text(data.especie);
      $('#carroceriaConsulta').text(data.carroceria);
      $('#categoriaConsulta').text(data.categoria);
      $('#combustivelConsulta').text(data.combustivel);
      $('#marca_modeloConsulta').text(data.marca_modelo);
      $('#ano_fabricacaoConsulta').text(data.ano_fabricacao);
      $('#ano_modeloConsulta').text(data.ano_modelo);
      $('#corConsulta').text(data.cor);
      $('#lotacaoConsulta').text(data.lotacao);
      $('#capacidadeConsulta').text(data.capacidade_carga);
      $('#potenciaConsulta').text(data.potencia);
      $('#cilindradasConsulta').text(data.cilindradas);
      $('#span_restricao_1_Consulta').text(data.restricao_1);
      $('#span_restricao_2_Consulta').text(data.restricao_2);

      $('#modalCarregando').modal('hide');
      $('#formConsulta').trigger("reset");
      $("#modalConsulta").modal("hide");
      $("#modalConsulta").modal("show");
    });

    posting.error(function(data){
      console.log(data);
      $('#modalCarregando').modal('hide');
      $('#modalErroTitle').text(data.responseJSON.message);
      $('#formConsulta').trigger("reset");
      $('#modalErro').modal('show');
    })
  });


  $('#formCadastro').on('submit', function(e){
    e.preventDefault();

    var url = "/cadastro";

    var posting = $.post( url, {login:$('#loginCadastro').val(), senha:$('#senhaCadastro').val()} );

    $('#modalCarregando').modal('show');

    posting.done(function( data ) {
      console.log(data);
      $('#modalCarregando').modal('hide');
      $('#modalSucessoTitle').text(data.message);
      $('#formCadastro').trigger("reset");
      $('#modalSucesso').modal('show');
    });

    posting.error(function(data){
      $('#modalCarregando').modal('hide');
      $('#modalErroTitle').text(data.responseJSON.message);
      $('#formCadastro').trigger("reset");
      $('#modalErro').modal('show');
    })
  });



}(jQuery);
