import tornado.web
import simplejson
import urllib
import urllib2

from tornado import gen

class CadastroHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def post(self):
        login = self.get_argument("login", default=None)
        senha = self.get_argument("senha", default=None)

        url = 'https://apr.caiorondon.com.br/cadastro'

        values = {
            'login' : login,
            'senha': senha,
        }

        data = urllib.urlencode(values)
        response = urllib2.urlopen(url, data)
        page = response.read()
        self.set_status(200)
        self.write(simplejson.loads(page))
        return
