import tornado.web
import simplejson
import urllib
import urllib2

import simplejson
from tornado import gen

class ConsultaHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def post(self):
        # preenche campos
        login = self.get_argument("login", default=None)
        senha = self.get_argument("senha", default=None)
        cpf_cnpj = self.get_argument("cpf_cnpj", default=None)
        renavam = self.get_argument("renavam", default=None)

        url = 'https://apr.caiorondon.com.br/consulta'

        values = {
            'login' : login,
            'senha': senha,
            'cpf_cnpj': cpf_cnpj,
            'renavam': renavam
        }
        try:
            data = urllib.urlencode(values)
            response = urllib2.urlopen(url, data)
            page = response.read()
            self.set_status(200)
            self.write(simplejson.loads(page))
        except urllib2.HTTPError as err:
            response = None

            if err.code == 404:
                response = {
                    "status":"error",
                    "message":"not found"
                }

            elif err.code == 401:
                response = {
                    "status":"error",
                    "message":"invalid user or password"
                }
                
            elif err.code == 400:
                response = {
                    "status":"error",
                    "message":"invalid or missing parameters"
                }

            print(err.code)
            self.set_status(err.code)
            self.write(response)
        return
