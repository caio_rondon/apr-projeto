# encoding: utf-8
# Criado por Caio Rondon, Laboratorio de APR - Projeto final - 2017.1
import os
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.wsgi

from handlers.main_handler import MainHandler
from handlers.consulta_handler import ConsultaHandler
from handlers.cadastro_handler import CadastroHandler
    
import sys

def create_web_server():
    # Roteamento para as diferentes URIs
    handlers = [
        (r"/", MainHandler),
        (r"/consulta", ConsultaHandler),
        (r"/cadastro", CadastroHandler),
        (r'/(.*)', tornado.web.StaticFileHandler, {'path': os.path.join(os.path.dirname(__file__), 'static')}),
    ]

    # Configurações da aplicação
    settings = dict(
        #template_path='templates/'
    )

    return tornado.web.Application(handlers, **settings)


if __name__ == '__main__':
    # Le a porta a ser usada a partir da configuracao lida
    # http_listen_port = ConfigHandler.http_listen_port
    print("Iniciando servico")
    http_listen_port = sys.argv[1]
    web_app = create_web_server()
    ioloop = tornado.ioloop.IOLoop.instance()
    # Inicia servidor web
    web_app.listen(http_listen_port)
    print("started")
    ioloop.start()
